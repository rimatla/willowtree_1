import {handleFetch} from './FetchData';
import {shuffle} from './ShuffleFive';


let app = () => {
    //fetch ajax data upon click
    document.getElementById('start-game').addEventListener('click', handleFetch);
    document.getElementById('play-matt').addEventListener('click', shuffle);
};
app();


